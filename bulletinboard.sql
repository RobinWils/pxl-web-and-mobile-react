SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `category` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `upvotes` int(11) NOT NULL,
  `downvotes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `post` (`id`, `title`, `content`, `category`, `user_id`, `upvotes`, `downvotes`) VALUES
(2, 'New AI program', '# A new dangerous AI program was made. \n Be careful what you wish for because it might have some bad sides aswell.', 'tech', 2, 11, 2),
(3, 'Free software', 'Free software is in fact important. \n **Stallman was right.** \n Too bad that this project is not GPL licensed', 'tech', 2, 10, 1),
(4, 'Server down', 'Our main server went down so the backup server is working now. \n We will work on the server issues tomorrow.', 'tech', 1, 11, 1);

CREATE TABLE `reaction` (
  `id` int(11) NOT NULL,
  `reaction` varchar(1000) NOT NULL,
  `token` varchar(275) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `reaction` (`id`, `reaction`, `token`, `post_id`) VALUES
(1, 'Epic', '5bb5e4ab75c37', 3),
(2, 'Epic', '5bb5e4b469740', 2),
(3, 'Epic', '5bb5e4be046d0', 4),
(4, 'Meh', '5bb5e4c0800af', 2),
(5, 'Epic', '5bb5e4c2a034b', 3),
(6, 'Epic', '5bb5e4c51423d', 2),
(7, 'Meh', '5bb5e4c71132b', 2),
(8, 'Epic', '5bb5e4c92128d', 3),
(9, 'Epic', '5bb5e4cb3c36f', 2),
(10, 'Epic', '5bb5e4ce560be', 2),
(11, 'Epic', '5bb5e4d10d2b8', 2),
(12, 'Epic', '5bb5e4d372218', 4),
(13, 'Epic', '5bb5e4d5bb089', 2),
(14, 'Epic', '7b4c806efd6006b1894e2f801a0a2398', 3),
(15, 'Epic', '7ad49f92b2b6de1c10a9225f8195a6c6', 2),
(16, 'Epic', 'e5a4e91d846da5b94b3d341cb70e1385', 4),
(17, 'Epic', 'dd67a1e4aa18ac7c87bc86b32a5d0c3f', 2),
(18, 'Epic', 'c11205e0bd18427cadfb30fead18f67d', 3),
(19, 'Meh', 'c6f43de0ca6fedb6e55d9ea4c255a78a', 2),
(20, 'Epic', '16195025965bb5e653beeb14.66666103', 4),
(21, 'Epic', '19557570185bb5e65a5d00d5.86900902', 2),
(22, 'Epic', '7094972775bb5e65f57f708.49889981', 3),
(23, 'Epic', '8785947435bb5e6630d55b7.66407103', 2),
(24, 'Meh', '18087009825bb5e665632931.55012942', 2),
(25, 'Epic', '11707725135bb5e66c4e1dd6.65837716', 4),
(26, 'Epic', 'MKbppKL8GVbrkCnOvouPVgCxGVkx6X9LC4LcFEyVsCJABfWPtg', 3),
(27, 'Epic', 'lj79JTXwMcukBetS3ymERrusnAepTp74fzmVs5C9fHLAGgCZ0Fopzrio9oY3mKAv', 4),
(28, 'Epic', 'ZnNJrw8y9cYCS2I36PRTiTojNwnsTezHXMQXwnrOCIGAP50XNM82FWWKpMUO5', 2),
(29, 'Epic', 'fSjDNjAO5Hvz22I4B7qOI7nQVNc4Ty3XN7IChJEiVDagPlFVm8UcdtHOCsEE6DTAg2x0hAtq1', 2),
(30, 'Meh', 'gvAFJm5KKZ27cCZz4z8BbWnjqGoCUAdK1B8itxXW4uM9Xo5cS', 2),
(31, 'Epic', 'sneOHlb11WkprhEpFC7TKgifVCp42JZEUgM72iDSXoO6ljZyvleA8pEI', 4),
(32, 'Epic', '5bb5e8d474874', 3),
(33, 'Epic', '5bb5e8e031cee', 2),
(34, 'Epic', '5bb5e90b6ed06', 3),
(35, 'Meh', '5bb5e92cb71dc', 2),
(36, 'Epic', '5bb5e9325deeb', 4),
(37, 'Epic', 'Lsr7sqhmR0NYRZKkghiEEr7sisx00anqlnuKA5C8QhO', 2),
(38, 'Meh', '0lMqugwiTvbCzd1hcLrnhQv9Pc5SyAHRVi2bOufdVp5PzTU5If8oS', 2),
(40, 'Meh', 'DXLfGLYDe2B3WK7GpVB5L74sIStCa3tJe0gTv9cK0xSSaCqKHnW6b6ucPffjr2dfQX50FBNj9dwRfWVs8eQHX8AEvqEh', 3),
(41, 'Meh', 'eNakQsf4Ve81EUKq5sg76XZh8wIp1BAR5cFajWDzEb5Bxc1c8a9un9s70b4', 3),
(53, 'Wow that are some vague reactions. I guess that this is reaction is vague too though...', 'f1OD5neLf5BbVrKYp6WXBYrcVsQBcv5lM7N76sgNJIqEKJ4GAa34Zlcnair0FrGPswYKK7e2as246ENmGUhhkuNftuKqVMnTFuTpKdmXgS4RCrEpqBWQP3klUZibbg7uILIaU8CkIdsZsdNSqanU8DvPATfLg47mum8wYuPOtzf1fchhxkfcaBnBmSyRSN3YNeIGUrN8ieCZza9R1a9wD3ZNIiaiddZAMwlVSuT2PjvzUXygdJ4oo5bc05c35862d8', 3),
(54, 'Wow that are some vague reactions. I guess that this is reaction is vague too though...', 'VRPD55LZA8jIw8Hs5VzFQRfbGRE6U8Y2mnpnsyzg8dftz75dd9Bn4zdqEeP0ZbSL9KVaZg0tIlUdmLAYcrv5KF0qhpjOcgqX20wJl0KRiyyLibtWgVGeIxbNimXVVp4GNepkga0FvOmFRCM3Mh2JvupF5ZKjgyfNsKAnr0pWC2QDsdjxeOJxLKfD28PIDMmrKbF64w1X6RILMiCSC5dnEiicaEctuF1Mi5v7fvD4O3psjUOCBNA3F5bc05ffac18d8', 4),
(55, 'Wow that are some vague reactions. I guess that this is reaction is vague too though...', 'wrUMamrCA8TUs8fAy6eG5vcGeqmlGlOLKftqoINw7I46SirXrjv6iwBbLuVAgRLZbPaaw4Z1IceHKFEODyHHfTTaD8wRXHNMayVpdOSskMwe8JTaIOcoWvvkhDzlON4oIUu4qoXwBE7NU2TZtwV1KCFDjoYLzfbsqsCNkTpfUJTmTZ8lDiQs3a2cJzjr9b7NCEgJ7yzAQqkIrRYfvsHzrKKIvyjB5sQ3JEluRf2zXqkcMNXCPTGCF5bc0607f9870a', 2),
(56, 'Meh', 'w1zOWHVE4XliJiwFCQm0bO2jCeqVQuuy1AnqhiZaYqnvlQq3UAt5oGrf1cJBpX19kkSBiKCDtKBR2t94R9TxpLTeUCzN1RVC8FUPiTFCS4CK8qsTi8nVXISpVw67aj82Ynv9FUUfQKKRtbuKocah859XCvVHWMUeC0Tx8qvXS0uXNkgYx1FNSlo5zPOfL8D2ENOgzHEASHMjFjCS5VoLvspysgFhqshr8PvUSvnVjuIfGe3YqTDmD5bc06967c31fe', 3),
(57, 'Meh', 'uVdjBt99kE2X5NkJoErvikDWutrxUmBpzkJV2fgKOvllMNJfwR9N9Crn27cYTtiLR7CezBZqg1hc00eboaRo6RlKF2BYh38XClfsEIhkzW2Dnph7gfPtS44MSTipTXF5HJPC6Jje1I7oGJDtQaxRPDPBQb6p80hzcsG1U9717W3fcSj6WShTUmbUtnvk2tbYhgk4H6sg6KPjPCGb9mxQDPLaHbGDAVgKc0jstZiGWS8OBLIGFVIoo5bc06c77e7c9c', 2),
(58, 'Meh', 'GAtzNktUNISZhRs8yjljU2QRWdUFe8IwAjzkgQelb0OAySXEp59iHn1RXvVG7xYleZB3UKUX0WiWM2kRhOZk5b2DIWMQ3rD6BaX77tHiLQWSQmztouXfpywPNNIZRYskDnmOqxGlRvN3WKJNpkYkm76xsJbhwzpcvGEkRjPfqxVR4JF9LsCkYqj8n6vGMenMktXHmAVDkt50JbFJzpF6mqC3s6jKvSA4GqdbNnk7qf5QKWROX12cJ5bc06c96954f5', 2);



CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mailCategories` varchar(1000) NOT NULL,
  `mailReactionsIsEnabled` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `role`, `username`, `password`, `email`, `mailCategories`, `mailReactionsIsEnabled`) VALUES
(1, 'anon', 'user1', 'weakPassword', 'example@gmail.com', 'None', 1),
(2, 'anon', 'user2', 'weakPassword', 'example@protonmail.com', 'Tech', 0),
(3, 'admin', 'admin', '$2y$10$biekO1TSjactlAZw1eB7AueF6scxfgRa/NyPBUQYAvCzjL/Lu3aHG', 'example@hotmail.com', 'Tech,News', 1);

ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_fk` (`user_id`);

ALTER TABLE `reaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id_fk` (`post_id`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `reaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `post`
  ADD CONSTRAINT `user_id_fk0` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `reaction`
  ADD CONSTRAINT `post_id_fk0` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;